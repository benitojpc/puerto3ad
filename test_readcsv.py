from services.readcsv_service   import CSVService 

def main():
    registros = CSVService.read("csvs/usuarios.csv")
    
    for reg in registros:
        for k in reg.keys():
            print(f'{k:>20}: {reg[k]}')
        print('\n')


if __name__ == "__main__":
    main()