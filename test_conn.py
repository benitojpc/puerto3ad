from modelos.UserAD             import UserAD
from services.connect_ad        import ConnAD
from services.users_service     import UsersService

def encontrar_usuarios():
    registros = ConnAD().find_all_users("description=Seguridad")
    for uad in registros:
        uad.show()
        
def tabla_usuarios(): 
    UsersService.listar_usuarios()

def encontrar():
    registro = ConnAD().find_user("benito palacios")
    registro.show()
    
if __name__ == "__main__":
    #encontrar()
    tabla_usuarios()