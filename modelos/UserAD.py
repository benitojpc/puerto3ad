class UserAD:
    
    def __init__(self):
        self._attributes = {
                            "accountExpires"    :"Expira",
                            "cn"                :"Common Name",
                            "description"       :"Description",
                            "displayName"       :"Display Name",
                            "distinguishedName" :"DN",
                            "givenName"         :"Given Name",
                            "lastLogon"         :"Last Logon",
                            "lastLogonTimestamp":"Last Logon Time",
                            "memberOf"          :"Member of",
                            "name"              :"Name",
                            "pwdLastSet"        :"Password Last Set",
                            "sAMAccountName"    :"sAMAccount Name",
                            "sn"                :"Surname",
                            "userPrincipalName" :"User Principal Name",
                            "whenChanged"       :"When changed",
                            "whenCreated"       :"When created",
                        }
        self._values = {}
        
    def _init_values(self):
        for k in self._attributes.keys(): 
            self._values[k] = None
        
    def get_attrs(self):
        return self._attributes.keys()
    
    def get_value(self, key):
        return self._attributes[key]
    
    def set_value(self, key, value):
        self._values[key] = value
        
    def show(self):
        for k in self._attributes.keys():
            if self._values[k] is not None:
                if k == "memberOf":
                    primero = True
                    grupos  = self._values[k]
                    for g in grupos:
                        if primero:
                            print(f'{self._attributes[k]:>20}: {g}')
                            primero = False
                        else:
                            print(f'{" ":>20}  {g}')
                else:
                    print(f'{self._attributes[k]:>20}: {self._values[k]}')