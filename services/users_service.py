from modelos.UserAD     import UserAD 
from .connect_ad        import ConnAD
from .readcsv_service   import CSVService

class UsersService:
    
    @staticmethod 
    def buscar_usuario(uname): 
        uad = UserAD()
        keys = uad.get_attrs()
        reg = ConnAD().find_user(uname, keys)
        if reg is not None:
            for k in keys:
                uad.set_value(k, reg[k])
            uad.show()
            
    @staticmethod 
    def agregar_nuevos_usuarios():
        registros = CSVService.read("csvs/usuarios.csv")
        for reg in registros: 
            uad = UsersService()._crear_usuario(reg) 
            
    @staticmethod
    def listar_usuarios(): 
        format_string = '{:45}  {:19}  {:19}  {}'
        print(format_string.format('User', 'Last Login', 'Expires', 'Description'))

        registros = ConnAD().find_all_users()
        for e in registros:
            print(format_string.format(str(e._values["name"])[:45], 
                                        str(e._values["lastLogon"])[:19], 
                                        str(e._values["accountExpires"])[:19], 
                                        str(e._values["description"])))
        
    @staticmethod 
    def _crear_usuario(reg):
        samacc = UsersService()._crear_samacc(reg['nombre'],reg['apellido1'],reg['apellido2'])
        print(f'{reg["apellido1"]} {reg["apellido2"]},{reg["nombre"]}: -> {samacc}')
        ConnAD().add_user(samacc, reg['nombre'],f"{reg['apellido1']} {reg['apellido2']}", reg['memberof'])
        return None
    
    @staticmethod 
    def _crear_samacc(nom, ape1, ape2):
        particulas = ['la','de', ' ', '']
        anom = nom.lower().split(' ')
        if len(anom) > 2:
            primero = True
            for s in anom:
                if primero:
                    nom = anom[0][0].upper()
                    primero = False
                else:
                    if s not in particulas:
                        nom += f'{s[0].upper()}{s[1:]}'
        elif len(anom) == 2:
            nom = f'{(anom[0][0].upper())}{anom[1][0].upper()}{anom[1][1:]}'
        else:
            nom = f'{anom[0][0].upper()}{anom[0][1:]}'
            
        apel = f'{ape1[0].upper()}{ape1[1:].lower()}'
        samacc = f'{nom} {apel}'
        samacc = UsersService()._buscar_samacc(samacc, ape2[0].upper())
        return samacc
    
    @staticmethod 
    def _buscar_samacc(samacc, inicial_ape2):
        paso    = 1
        orden   = 1
        result  = None
        
        while True:
            if paso == 1:
                result = ConnAD().find_user(samacc, ['sAMAccountName'])
                paso += 1
            else:
                if paso == 2:
                    samacc += inicial_ape2
                    result = ConnAD().find_user(samacc, ['sAMAccountName'])
                    paso += 1
                else:
                    samacc += str(orden)
                    result = ConnAD().find_user(samacc, ['sAMAccountName'])
                    orden += 1
            if result is None:
                break
        return samacc