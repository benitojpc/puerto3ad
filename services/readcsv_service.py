import csv

class CSVService:
    
    COLUMNAS    = []
    REGISTROS   = []
    
    @staticmethod
    def read( fcsv ):
        with open( fcsv, mode='r') as file:
            csvf = csv.reader( file )
    
            primera = True        
            for line in csvf:
                if primera:
                    primera = False 
                    CSVService._read_cols(line)
                else:
                    CSVService.REGISTROS.append(CSVService._set_values(line))
        return CSVService().REGISTROS
                
    @staticmethod 
    def _set_values(line):
        datos   = {}
        valores = line[0].split(";")
        for i in range(len(valores)):
            datos[CSVService().COLUMNAS[i]] = valores[i]
        return datos
    
    @staticmethod 
    def _read_cols(line):
        CSVService.COLUMNAS = line[0].split(";")
        #for col in CSVService.COLUMNAS:
        #    CSVService().DATOS[col] = None