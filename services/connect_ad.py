#from msilib.schema      import Error
from ldap3              import Server 
from ldap3              import Connection
from ldap3              import SUBTREE
from ldap3              import MODIFY_REPLACE

from ldap3.extend.microsoft.addMembersToGroups import ad_add_members_to_groups as addUsersInGroups

from modelos.UserAD import UserAD

class ConnAD:
    
    AD_HOST     = "192.168.5.21"
    AD_PORT     = 391
    AD_USER     = "benito palacios"
    AD_PASS     = "bjpc@1961"
    AD_DOMAIN   = "iipp.int"
    AD_BASE     = "dc=iipp,dc=int"
    AD_OBJ_CLASS= ['top', 'person', 'organizationalPerson', 'user']
    
    AD_BASE_USER= f"OU=PUERTO3,OU=Comunes,OU=USUARIOS,{AD_BASE}"
    AD_GROUPS   = [
                    "CN=map_conecta,OU=Grupos Comunes,OU=USUARIOS,DC=iipp,DC=int",
                    "CN=Puerto3,OU=PUERTO3,OU=Comunes,OU=USUARIOS,DC=iipp,DC=int"
                    ]
    
    CONN        = None
    
    @staticmethod 
<<<<<<< HEAD
    def find_user(uname, attrs=['*']): 
        uad     = UserAD() 
        attrs   = uad.get_attrs()
=======
    def find_user(uname, attrs=['sAMAccountName','cn'], msg=False): 
>>>>>>> 038dbedef59983c94cf6baea527f95dbefbf2a22
        filter = f'(&(objectclass=user)(sAMAccountName={uname}))'
        if ConnAD.CONN == None:
            ConnAD._connect()
        ConnAD.CONN.search(
                            search_base=ConnAD.AD_BASE, 
                            search_filter=filter,
                            search_scope=SUBTREE,
                            attributes=attrs)
        if len(ConnAD.CONN.entries) == 1:
            for e in ConnAD.CONN.entries:
                for attr in attrs:
                    uad.set_value( attr, e[attr])
                return uad
        else:
            if msg:
                print(f'No existe el usuario: {uname}, o existe más de una coincidencia')
            return None
    
    @staticmethod 
    def find_all_users( filtro="" ): 
        datos = []
        filter = f'(&(objectclass=user)(sAMAccountName=*))' if (filtro == "") else f'({filtro})'
        if ConnAD.CONN == None:
            ConnAD._connect()
        attrs = UserAD().get_attrs()
        ConnAD.CONN.search(
                            search_base=ConnAD.AD_BASE, 
                            search_filter=filter,
                            search_scope=SUBTREE,
<<<<<<< HEAD
                            attributes=attrs)
        
        try:
            for e in ConnAD.CONN.response:
                uad = UserAD()
                for attr in attrs:
                    uad.set_value( attr, e["attributes"][attr])
                datos.append( uad )
        except:
            pass
        return datos
=======
                            attributes=['sAMAccountName','cn'])
        for e in ConnAD.CONN.response:
            print(e)
            
    @staticmethod 
    def add_user(samacc, name, surname, groups):
        user_dn    = ConnAD()._get_dn(samacc)
        user_attrs = ConnAD()._get_attrs(samacc, name, surname)
        
        result = ConnAD().CONN.add(
                                    dn=user_dn, 
                                    object_class = ConnAD().AD_OBJ_CLASS, 
                                    attributes= user_attrs)
        if not result:
            msg = f"ERROR: User {samacc} was not created: {ConnAD().CONN.result.get('description')}"
            raise Exception(msg)
            
        # desbloquear y establecer la contraseña
        ConnAD().CONN.extend.microsoft.unlock_account(user=user_dn)
        ConnAD().CONN.extend.microsoft.modify_password(user=user_dn,
                                                        new_password="nuevas",
                                                        old_password=None)
                                                        
        # habilitar la cuenta 
        enable_account = {"userAccountControl" : (MODIFY_REPLACE, [512])}
        ConnAD().CONN.modify(user_dn, changes=enable_account)
        
        # añadir los grupos
        user_groups = ConnAD()._get_groups(groups)
        for g in user_groups:
            for i in range(len(g)):
                #print(f'{g[i]:>20}')
                addUsersInGroups(ConnAD.CONN, user_dn, g[i])
    
    @staticmethod 
    def _get_dn(samacc):
        return f'CN={samacc},{ConnAD().AD_BASE_USER}'
    
    @staticmethod 
    def _get_attrs(samacc, name, sn):
        name    = ConnAD()._capitalize(name)
        surname = ConnAD()._capitalize(sn)
        
        return {"displayName": samacc, "sAMAccountName": samacc,
                "userPrincipalName": f'{samacc}@{ConnAD().AD_DOMAIN}',
                "name": f'{surname}, {name}', "givenName": name, "sn": surname.rstrip()}
        
    @staticmethod 
    def _capitalize(texto):
        particulas  = ["de","la", "los", "  ", ""]
        atexto      = texto.split(" ")
        new_texto   = ""
        
        for s in atexto:
            if s.lower() in particulas:
                new_texto += f'{s.lower()} '
            else:
                new_texto += f'{s[0].upper()}{s[1:].lower()} '
        return new_texto.rstrip()
>>>>>>> 038dbedef59983c94cf6baea527f95dbefbf2a22
        
    @staticmethod
    def _connect():
        srv = Server(ConnAD.AD_HOST)
        ConnAD.CONN = Connection(srv, ConnAD.AD_USER, ConnAD.AD_PASS, auto_bind=True)
        
    @staticmethod 
    def _check_conn():
        print(f'Comprobación de la conexión a A.D.')
        print(ConnAD.CONN)
        
    @staticmethod 
    def _get_groups(grps):
        grupos = grps.split(":")
        new_grps = []
        
        tmp_grp  = [x for x in ConnAD().AD_GROUPS]
        for g in grupos:
            tmp_grp.append(f'CN={g},OU=PUERTO3,OU=Comunes,OU=USUARIOS,DC=iipp,DC=int')
        new_grps.append(tmp_grp)
            
        return new_grps